FROM rocker/r-ver

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update

## packages needed for basic shiny functionality
RUN Rscript -e "install.packages(c('shiny', 'ggplot2', 'shinyalert', 'DT', 'data.table', 'feather', 'dplyr'),repos='https://cloud.r-project.org
')"

# install the shiny application
RUN Rscript -e "install.packages('https://gitlab.com/avelt/Vitis_ID_converter/-/raw/master/VitisIDconverter_0.1.0.tar.gz',repos=NULL,type='sour
ce')"

# set host and port
COPY Rprofile.site /usr/lib/R/etc/
EXPOSE 3838

CMD ["R", "-e VitisIDconverter::shiny_app()"]
